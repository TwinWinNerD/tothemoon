define(['lib/react', 'components2/rocket', 'components2/controls', 'components2/log_chat_selector', 'components2/players', 'components2/payout'],
  function(React, Rocket, Controls, LogChatSelector, Players, Payout) {
    var D = React.DOM;

    return React.createClass( {
      displayName: 'Game',

      propTypes: {
        engine: React.PropTypes.object.isRequired
      },

      render: function() {
        if (!this.props.engine.isConnected)
          return D.div(({className: 'loading'}), 'Connecting to server..');

        var divArgs = [{id: "rocket", className: 'rocket' }, D.div({ className: 'ship' }), D.div({ className: 'rocket-flame' })]
        for (i = 0; i < 100; i++){
          divArgs.push(D.div({ className: 'explosion-particle' }));
        }
        return D.div({ className: 'inner-wrapper' },
          D.div({ className: 'col-left' },
            D.div({ className: 'rocket-outer' },
              D.div({ className: 'rocket-inner' },
                D.div.apply(null, divArgs),
                D.div({id: "game-multiplier", className: 'multiplier'})
              )
            ),
            Rocket({ engine: this.props.engine }),
            Controls({ engine: this.props.engine })
          ),
          D.div({ className: 'col-right' },
            D.div({ className: 'players' },
              Players({ engine: this.props.engine })
            ),
            D.div({ className: 'log-chat' },
              LogChatSelector({ engine: this.props.engine }) 
            )
          )
        )
      }
    });
  }
);
