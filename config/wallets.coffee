fs        = require "fs"
AppHelper = require "../lib/app_helper"

wallets = {}

for currency in AppHelper.getCurrencies(true)
  walletType = currency.toLowerCase()
  options = GLOBAL.appConfig().wallets[walletType]
  path = "#{process.cwd()}/lib/crypto_wallets/#{walletType}_wallet.js"
  if fs.existsSync path
    Wallet = require path
  else
    Wallet = require "#{process.cwd()}/lib/crypto_wallet"
  wallets[walletType] = new Wallet options

exports = module.exports = wallets