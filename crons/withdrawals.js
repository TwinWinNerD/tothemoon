(function() {
  var CronJob, TransactionsHelper, async, environment;

  async = require("async");

  GLOBAL.appConfig = require("../config/config");

  environment = process.env.NODE_ENV || "development";

  if (environment === "production") {
    require("../config/logger");
  }

  GLOBAL.db = require("../server/database");

  TransactionsHelper = require("../lib/transactions_helper");

  GLOBAL.wallets = require("../config/wallets");

  CronJob = require("cron").CronJob;

  new CronJob({
    cronTime: "*/50 * * * * *",
    start: true,
    onTick: function() {
      var cronInProgress;
      if (!cronInProgress) {
        cronInProgress = true;
        TransactionsHelper.processedUserIds = [];
        return GLOBAL.db.getPendingWithdrawals(function(err, withdrawals) {
          return async.mapSeries(withdrawals, TransactionsHelper.processWithdrawal, function(err, result) {
            if (err) {
              console.log(err);
            }
            console.log("" + result + " - " + (new Date()));
            return cronInProgress = false;
          });
        });
      }
    }
  });

  console.log("Withdrawals cron started");

}).call(this);
