async              = require "async"
GLOBAL.appConfig   = require "../config/config"
environment        = process.env.NODE_ENV or "development"
require "../config/logger"  if environment is "production"
GLOBAL.db          = require "../server/database"
TransactionsHelper = require "../lib/transactions_helper"
GLOBAL.wallets     = require "../config/wallets"
CronJob            = require("cron").CronJob

transactionsStart = 0
transactionsCount = 300
cronInProgress    = false

new CronJob
  cronTime: "*/10 * * * * *"
  start: true
  onTick: ()->
    if not cronInProgress
      cronInProgress = true
      GLOBAL.wallets["btc"].getTransactions "*", transactionsCount, transactionsStart, (err, transactions = [])->
        console.error err if err
        async.mapSeries transactions, TransactionsHelper.loadDeposit, (err, result)->
          console.log err  if err
          console.log "BTC transactions synced - #{new Date()}"
          cronInProgress = false

console.log "Deposits cron started"