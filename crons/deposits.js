(function() {
  var CronJob, TransactionsHelper, async, cronInProgress, environment, transactionsCount, transactionsStart;

  async = require("async");

  GLOBAL.appConfig = require("../config/config");

  environment = process.env.NODE_ENV || "development";

  if (environment === "production") {
    require("../config/logger");
  }

  GLOBAL.db = require("../server/database");

  TransactionsHelper = require("../lib/transactions_helper");

  GLOBAL.wallets = require("../config/wallets");

  CronJob = require("cron").CronJob;

  transactionsStart = 0;

  transactionsCount = 300;

  cronInProgress = false;

  new CronJob({
    cronTime: "*/10 * * * * *",
    start: true,
    onTick: function() {
      if (!cronInProgress) {
        cronInProgress = true;
        return GLOBAL.wallets["btc"].getTransactions("*", transactionsCount, transactionsStart, function(err, transactions) {
          if (transactions == null) {
            transactions = [];
          }
          if (err) {
            console.error(err);
          }
          return async.mapSeries(transactions, TransactionsHelper.loadDeposit, function(err, result) {
            if (err) {
              console.log(err);
            }
            console.log("BTC transactions synced - " + (new Date()));
            return cronInProgress = false;
          });
        });
      }
    }
  });

  console.log("Deposits cron started");

}).call(this);
