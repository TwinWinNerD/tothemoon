
var assert = require('assert');
var Emailer = require('../lib/emailer');

exports.contact = function(from, content, user, callback) {
    var data = {
        from: from,
        content: content
    }
    var options = {
        to: {
            email: GLOBAL.appConfig().emailer.contact_email
        },
        subject: "Contact form sumission on ToTheMoon.is",
        template: "contact"
    }
    var emailer = new Emailer(options, data);
    emailer.send(function (err, result) {
        if (err) console.error(err);
    });
    return callback();
};

exports.passwordReset = function(to, recoveryId, callback) {
    var passUrl = "/reset/" + recoveryId
    var data = {
        pass_url: passUrl
    }
    var options = {
        to: {
            email: to
        },
        subject: "ToTheMoon.is - Reset Password Request",
        template: "password_reset"
    }
    var emailer = new Emailer(options, data);
    emailer.send(function (err, result) {
      if (err) console.error(err);
    });
    return callback();
}
