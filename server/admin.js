var assert = require('assert');
var async = require('async');
var database = require('./database');
var AsyncCache = require('async-cache');
var timeago = require('timeago');

var stats = new AsyncCache({
    max: 1,
    maxAge: 1000 * 60 * 1,
    load: function (key, cb) {
        console.log('loading ', key, cb);
        database.getSiteStats(function(err, results) {
            if (err) return cb(err);

            results.generated = new Date();
            cb(null, results);
        });
    }
});

exports.stats = function(req, res, next) {
    var user = req.user;
    assert(user.admin);

    stats.get('stats', function(err, results) {
        if (err)
            return next(new Error('Unable to get site stats: ' + err));

        res.render('stats', {user: user, generated: timeago(results.generated), stats: results});
    });
};

exports.giveAway = function(req, res) {
    var user = req.user;
    assert(user.admin);

    res.render('giveaway', {user: user});
};

exports.giveAwayHandle = function(req, res, next) {
    var user = req.user;
    assert(user.admin);

    console.log('Give away referer is: ', req.get('Referer'));

    if (process.env.NODE_ENV === 'production') {
        var ref = req.get('Referer');
        if (!ref) return next(new Error('possible xsfr'));

        if (ref.lastIndexOf('http://tothemoon.is/admin-giveaway', 0) !== 0)
            return next(new Error('Bad referrer got: ' + ref));
    }

    var giveAwayUsers = req.body.users.split(/\s+/);
    var bits = parseFloat(req.body.bits);

    if (!Number.isFinite(bits) || bits <= 0)
        return next(new Error('problem with bits...'));

    var satoshis = Math.round(bits * 100);

    database.addRawGiveaway(giveAwayUsers, satoshis , function(err) {
        if (err) return res.redirect('/admin-giveaway?err=' + err);

        res.redirect('/admin-giveaway?m=Done');

    });
};
