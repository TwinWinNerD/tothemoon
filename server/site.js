var assert = require('assert');
var async = require('async');
var database = require('./database');
var AsyncCache = require('async-cache');
var timeago = require('timeago');

var stats = new AsyncCache({
    max: 1,
    maxAge: 1000 * 60 * 1,
    load: function (key, cb) {
        console.log('loading ', key, cb);
        database.getSiteStats(function(err, results) {
            if (err) return cb(err);

            results.generated = new Date();
            cb(null, results);
        });
    }
});

exports.index = function(req, res, next) {
    var user = req.user;

    stats.get('stats', function(err, results) {
        if (err) console.error('Unable to get site stats: ' + err);
        
        res.render('index', {user: user, generated: timeago(results.generated), stats: results || {}});
    });
};
