INSTALLATION
============

Debian/Ubuntu
-------------

These are instructions for running ToTheMoon locally on a Debian / Ubuntu machine.

### Distribution packages

You will need to install the Postgres DBMS and node.js. The `nodejs-legacy`
package installs `nodejs` but will additionally create a symlink from
`/usr/bin/node` to `/usr/bin/nodejs`.

    sudo apt-get install git npm postgresql nodejs-legacy

### Getting the sources

    git clone https://tothemoonis@bitbucket.org/tothemoonis/tothemoon.git
    cd tothemoon

### Create a database user and setup the tables

Create a user. It will prompt you for a password.

    sudo -u postgres createuser -P tothemoon

Create the database and setup the tables. The second command will prompt you
for the password again.

    sudo -u postgres createdb -O tothemoon tothemoondb
    psql -W -U tothemoon -d tothemoondb -h localhost -f server/schema.sql

Mac OS X
--------

These are instructions for running ToTheMoon locally on a Mac using homebrew.

### Install homebrew packages

    brew install git node npm postgresql

### Getting the sources

    git clone https://tothemoonis@bitbucket.org/tothemoonis/tothemoon.git
    cd tothemoon

### Create a database user and setup the tables

Create a user. It will prompt you for a password.

    createuser -P tothemoon

Create the database and setup the tables. The second command will prompt you
for the password again.

    createdb -O tothemoon tothemoondb
    psql -W -U tothemoon -d tothemoondb -h localhost -f server/schema.sql


Configuration
=============

### Installing node.js dependencies locally.

This will download and install all dependencies in the `node_modules` subdirectory.

    npm install

### Settings

Rename the `config.json.sample` to `config.json` and add the necessary settings.

### Wallet

Rename the `btc.json.sample` to `btc.json` and add the necessary settings in `config/wallets_config`.


Running
=======

You can run the server by using `npm start`. By default it will listen on port `3841`.
